.\"
.\" This is the manual page for gentoo. This page appeared first with
.\" release 0.9.22, so it should at least be current to that release.
.\"
.\" As with all other documentation in the gentoo project, it might
.\" end up being outdated pretty quickly...
.\"
.\" gentoo is (c) 1998-2016 by Emil Brink, Obsession Development.
.\"
.\" gentoo is freely distributable under the terms of the GNU General
.\" Public License as specified in the COPYING file included with the
.\" distribution.
.\"
.TH "gentoo" "1x" "June, 2016" "Obsession Development" ""
.SH NAME
gentoo \- A highly configurable file manager for X
.\"
.SH "SYNOPSIS"
gentoo [\-\-version] [\-\-locale-info] [\-\-root\-ok] [\-\-no\-rc] [\-\-no\-gtkrc] [\-\-no\-dir\-history] [\-\-left=path] [\-\-right=path] [\-\-run=ARG] 
.\"
.SH "DESCRIPTION"
gentoo is a file manager for Linux and compatible systems. It allows you
to interactively navigate your file system using the mouse, and also to
perform various fairly standard operations (such as copy, move, rename, ...)
on the files and directories contained therein.
.PP
.\"
gentoo always shows you the contents of two directories at once. Each of these is
displayed in its own scrollable list, called a pane. At any time, exactly one pane
is the
.I current
pane, and has a highlighted bar running across its top region. The current pane acts
as the source for all file operations, while the other pane is the destination. You can
select rows in panes using selection methods of varying complexity (from simply clicking
a row, to selecting rows by name using a regular expression). Once you have a selection,
you can click a button to perform some command on the selected files.
.PP
.\"
All file operations performed by gentoo are implemented natively. When you use gentoo
to copy a file, for example, gentoo does
.I not
simply execute the system's
.BR "cp" "(1L)"
command. Rather, gentoo contains its own code for opening source and destination files,
and then reading and writing the right amount of data between them. This way of doing
things makes gentoo independent of the availability of shell commands to do things.
.PP
.\"
gentoo incorporates a fairly powerful, object-oriented file typing and
styling system. It can use a variety of ways to determine the
.B type
of the files it is displaying. Each type is then linked to something called a
.B style,
which controls how rows of that type are rendered in panes. You can use this
system to control icons, colors, and various operations on the rows. For
example, it is easy to make gentoo display all PNG images in red, and to
invoke
.BR "The GIMP" (1)
on them when double-clicked.
.PP
.\"
A design goal with gentoo has been to provide full GUI configurability,
removing the need to edit a configuration file by hand and restart the program to
see the changes, as is otherwise common in many programs for Un*x. As a result
of this, gentoo features a Configuration dialog window where you can configure
most aspects of its operation directly, using the mouse and standard GUI widgets.
.PP
gentoo borrows its basic look'n'feel from the classic Amiga file manager
Directory OPUS, but is not a "clone" of any kind.
.\"
.SH "OPTIONS"
gentoo is not primarily driven by command line arguments, but the following
are available:
.TP
.B \-\-version
Causes gentoo to print its version number (a string of the form MAJOR.MINOR.MICRO,
like 0.20.7) to the standard output, and then exit successfully. Numbers having an
odd MINOR component indicate development versions of the program. So far, all
versions of gentoo have been classified as being development versions.
.\"
.TP
.B \-\-locale-info
Makes gentoo print a couple of localization settings, and then exit.
This is mostly useful during development and debugging, and not of a
lot of interest when just using the application.
.\"
.TP
.B \-\-root\-ok
Makes gentoo accept being run by the root user. Normally, this is not
allowed since it is considered a big threat to system security. Note
that gentoo has the ability to execute user-defined strings using the
.BR "execvp" (3)
function. This is generally considered harmful. However, if you
.B "really"
want to run gentoo while logged on as root, supplying this option
allows you to. It is
.B "not"
recommended, though.
.\"
.TP
.B \-\-no\-rc
Starts up gentoo without loading any configuration file. This makes
it run using the built-in defaults, which are
.B very
Spartan indeed. Seldom comfortable, but occasionally handy when trying
to determine if a problem is with the configuration or with the core code.
.\"
.TP
.B \-\-no\-gtkrc
Avoids loading the GTK+ RC file, thus disabling any widget customizations,
and forces all widgets to use the default GTK+ look.
.\"
.TP
.B \-\-no\-dir\-history
Avoids loading the file that holds the history, i.e. which directories have
been previously visited by the two panes. Very rarely needed, included mostly
for completeness' sake.
.\"
.TP
.B \-\-left, \-\-right (or \-1, \-2)
Sets the initial path for the left and right pane, respectively. If present,
the path specified with one of these options overrides any other path for
the pane in question. See below (Initial Directory Paths) for details.
.\"
.TP
.B \-\-run ARG (or \-rARG)
Runs ARG, a gentoo command. Commands specified this way are executed before
gentoo accepts any user input through the graphical interface, but after the
configuration file has been read in. You can use it many times in order to
make gentoo run a whole series of commands. Remember that gentoo's command
names are case-sensitive, and that built-in commands (like "About") always
begin with a capital letter.
\"
.PP
Any non-option command arguments will be silently ignored. If an argument "\-h" or
"\-\-help" is given, gentoo will give a summary of its supported command line options
and exit successfully. If an unknown option is given, or a option is missing a
required argument, gentoo will whine and exit with a failure.
.\"
.\"
.SH "BASIC USAGE"
When gentoo starts up, it will open up its single main window, which is split vertically
(or horizontally; it's configurable) down the middle, forming the two panes mentioned
above. It also contains a bank of buttons along the bottom.
.\"
.\"
.SS "Initial Directory Paths"
The actual paths shown in the two panes upon start-up can be controlled in various
ways. There are four ways of getting a path to show up in pane. In order of decreasing
priority, they are:
.TP
1. Command-line Argument
Using the \-\-left and \-\-right (or their short forms, \-1 and \-2) command-line arguments
overrides any other setting.
.TP
2. Configured Default Directory
If no command-line argument is present, and the "Default Directory" configuration option
is set, that directory is used.
.TP
3. Most Recently Visited Directory
If no default directory exists, the most recently visited directory is taken from the
directory history for each pane. This only works if a directory history file has been
found and loaded.
.TP
4. Current Directory
If all else fails, gentoo uses the current directory (".").
.\"
.\"
.SS "Navigating"
Navigating around the file system using gentoo is very simple. The two panes act
as independent views of the file system, and both are navigated in exactly the same way.
.PP
You can always see which directory a pane is showing by reading its path, shown in
the entry box below (by default--you can change the position to above) the pane.
.PP
To enter a directory, locate it in the pane and double click it with the left mouse
button. gentoo will read the directory's contents, and update the display accordingly.
.PP
There are several ways of going
.B "up"
in the directory structure. To enter the directory
containing the one currently shown (the current dir's
.BR "parent" "),"
you can: click the
parent button (to the left of the path entry box); hit
.B "Backspace"
on your keyboard; click the middle mouse button; select "Parent" from the pop-up
menu on the right mouse button, or click the downward arrow to the right of the path
box (this pops up the directory history menu), then select the second row from the top.
.\"
.\"
.SS "Selecting Files"
Before you can do anything to a file, you need to select it. All file-management commands
in gentoo act upon the current selection (in the current pane). There are several ways
of selecting files, but the most frequently used are mouse-based. Note that the word "file"
used below really should be taken to mean "file or directory", since selection doesn't
distinguish between the two.
.PP
.\"
To select a file (or directory), just point the mouse at the name (anywhere in the row
is fine), and click the left mouse button. The colors of the clicked row will change,
indicating that it is currently selected. To select more rows, keep the mouse button down,
and drag the mouse vertically. gentoo extends the selection, including all rows touched.
If you drag across the top or bottom border, the pane will scroll, trying to keep up.
This is a very quick and convenient way of selecting multiple files, as long as they are
listed in succession.
.PP
If you click again on an already selected file, you will unselect it. You can drag to
unselect several files, just as when selecting.
.PP
.\"
To select a sequence of files without dragging, first click normally on the first file that
you wish to select. Then release the mouse button, locate the last file in the sequence
(it can be either above or below the first one), hold down
.B "shift"
on your keyboard, and click the wanted file. gentoo now adds all files between the first
and the last to the current selection.
.PP
.\"
If you follow the instructions given above to select a sequence, but press
.B "control"
rather than
.B "shift"
before clicking the second time, gentoo will
.I "unselect"
the range of files indicated.
.PP
.\"
If you click on a file with the
.B "meta"
key held down (that's actually a key labeled
.BR "Alt" ","
located to the immediate left of the space bar, on my PC keyboard), gentoo will do something
cool: it will select (or unselect, it's a toggle just like ordinary selection) all files,
including the clicked one, that have the same
.B "type"
as the one you clicked. This can be used to select for example all PNG image files in a
directory even if you can only see one. Occasionally very useful.
.PP
.\"
If you click on a file with both the
.BR "shift" " and " "control"
keys held down, gentoo will toggle the selected state of all files having the same
file name
.I "extension"
as the one you clicked. This can sometimes be useful to select files that you don't
have a proper
.B "type"
defined for, as long as those files do share an extension, that is.
.\"
.SS "Changing Sort Order"
The files and directories listed in each of gentoo's two panes are always sorted on some
column: typically file name. You can chose to sort on some other field by clicking the appropriate
column title once. If you click on the field that is already current, the sorting will be reversed
(i.e., for names it will be Z-A rather than A-Z).
.PP
If your display includes icons, try sorting on that column: gentoo will then order each row according
to its File Style, grouping the rows based on their parent styles, all the way up to the root of
the Style tree. This means that, for example, JPEG and PNG pictures (both having an immediate parent
style of Image) will be shown together, and before all Text files (HTML, man pages and so on). It's
quite cool, really. :)
.\"
.SS "Executing Commands"
Commands are used to make gentoo
.B "do"
stuff. The typical command operates upon the set of selected files in the current
pane, so it's usually a good idea to first select some files. See the previous
subsection for details on how to select files. Once you have a bunch of files selected,
you need to tell gentoo which command to execute. There are several ways of doing
this.
.PP
.\"
Most basic file operations (e.g. copy, move, rename, and so on) are found on the (cleverly
labeled) buttons along the bottom of gentoo's main window. To copy a file, just select it,
then click the button labeled "Copy". It's really that simple. Most of these built-in
(or
.BR "native" ")"
commands automatically operate recursively on directories, so you could copy (or move)
a whole directory of files by just selecting it and then clicking "Copy".
.PP
.\"
If you can't see a button that does what you want to do, there's a chance that the
command exists, but isn't bound. Click the right mouse button in a pane, this opens up
the "pane pop-up menu". Select the "Run..." item. This opens up a dialog window showing
all available commands. Select a command, and click "OK" to execute it.
.\"
.\"
.SH "CONFIGURATION"
gentoo is a pretty complicated program; it has a rather large amount of configuration
data that it needs in order to be really useful. For example, my current personal
configuration file contains well over a thousand different configuration values.
.PP
.\"
To store this hefty amount of configuration data, gentoo uses a heavily structured
configuration file. In fact, the file is (or at least it should be) legal XML!
.PP
.\"
When new features are added to gentoo, they will typically require some form
of configuration data. This data is then simply added somewhere in the existing
configuration file structure. Effort is made to assign reasonable built-in default values
for all such new features, so older configuration files (that don't contain the
values required by the new features) should still work. The first time you hit
"Save" in the configuration window after changing your version of gentoo, your
personal configuration file will be updated to match the version of gentoo.
.PP
.\"
Describing how to go about configuring gentoo is too big a topic for a manual
page to cover. I'll just say that the command to open up the configuration
window is called "Configure". It is by default available on a button (typically
the top-right one), in the pane pop-up menu, and also by pressing the
.B "C"
key on your keyboard.
.PP
.\"
.\"
.SH "FILES"
.TP
.I "~/.config/gentoo/gentoorc"
A user's personal configuration file. When gentoo starts up, it will try to
load this file. If the file isn't found, the old name
.I "~/.gentoorc"
is tested, and if that also fails a site-wide configuration (see below) will be tried instead.
.\"
.TP
.I "/usr/local/etc/gentoorc"
This is the site-wide configuration file. If a user doesn't have a configuration
in his/her home directory, gentoo loads this file instead. The actual location
of this file is slightly system-dependent, the above is the default. As an end
user, you typically won't need to access this file manually.
.\"
.TP
.I "~/.config/gentoo/dirhistory"
This file contains lists of the most recently visited directories, for both
panes. These are the lists that appear in the drop-down menu when the arrow
next to the path entry box is clicked. Can be disabled in the Dir Pane configuration.
.TP
.I "~/.config/gentoo/gtkrc"
This file allows you to control the look of the widgets used by gentoo,
through the GTK+ style system. You can change the actual path in gentoo's
Configuration window, the above is the typical default for a modern Linux-based
system. If a file named gtkrc is not found in the configured path, the names
.I "gentoogtkrc"
and
.I ".gentoogtkrc"
(note the period), in that order, are also tested.
.\"
.TP
.I "/etc/passwd," "/etc/group"
These two files normally hold the system's password and group information.
These are (probably) the ones gentoo uses to map user IDs to login names,
to do tilde-expansion (mapping of user name to directory path), and to
map group IDs to group names.
.\"
That is
.I probably,
because gentoo doesn't actually refer to these files by name. Instead, it
uses the (BSD-style) API function calls
.BR "getpwent" "(3)"
and
.BR "getgrent" "(3)"
to access this information.
.\"
.TP
.I "/etc/fstab," "/proc/mounts," "(or /etc/mtab)"
These files contain data on available and mounted file systems. They
are read by gentoo's auto-mounting code. You can configure the exact
file names used, on the "Mounting" tab in the main configuration window.
Note that using
.I "/proc/mounts"
rather than
.I "/etc/mtab"
is recommended on Linux systems; they contain roughly the same data,
but the one in /proc is always up to date, and faster to read!
\"
.SH "BUGS"
All releases of gentoo numbered 0.x.y, where x (the so called
.B minor
version number) is odd, are to be considered
.B
development
releases, as opposed to
.B
stable
ones. This means that the software will probably suffer from bugs. If you find something
that you suspect is indeed a bug, please don't hesitate to contact the author!
For details on how to do this, see below.
.PP
.\"
If you're concerned about using potentially buggy and completely unwarranted
software to manage your precious files, please feel free
.B not
to use gentoo. The world is full of alternatives.
.PP
.\"
The chances that a bug gets fixed increase greatly if you report it. When reporting
a bug, you must describe how to reproduce it, and also try to be as detailed and
precise as possible in your description of the actual bug. If possible, perhaps
you should include the output of
.BR "gdb" "(1)"
(or whatever your system's debugger is called). In some cases it might be
helpful if you include the configuration file you were using when the problem
occurred. Before reporting a bug, please make sure that you are running a
reasonably recent version of the software, since otherwise "your" bug might
already been fixed. See below for how to obtain new releases.
.PP
.\"
Also, you should locate and read through the
.B BUGS
file distributed with gentoo, so you don't go through all this hassle just to
report an already known bug, thereby wasting everybody's time...
.\"
.\"
.SH "AUTHOR"
gentoo was written, from scratch, by Emil Brink. The first line of code was written
on May 15th, 1998. It is my first program to use the GTK+ GUI toolkit, my first
program to be released under the GPL, and also my first really major Linux
application.
.PP
.\"
The only efficient way to contact me (to report bugs, give praise, suggest
features/fixes/extensions/whatever) is by Internet e-mail. My address is
.RB "<" emil@obsession.se ">."
.B Please
try and include the word "gentoo" in the Subject part of your e-mail,
to help me organize my inbox. Thanks.
.\"
If you're really not in the mood for the direct feel of e-mail, the second best
choice for reporting bugs and making suggestions is the use the web-based bug
tracker at
.RB "<" https://sourceforge.net/p/gentoo/bugs/ ">."
Thanks for contributing.
.\"
.SH "ACKNOWLEDGMENTS"
The author wishes to thank the following people for their various contributions
to gentoo:
.\"
.TP
Johan Hanson (<johan@tiq.com>)
Johan is the man behind all icon graphics in gentoo, and also the author of
the custom widgets used in it. He also comes up with plenty of ideas for new
features and changes to old ones, some of which are even implemented. Johan
has stuff at <http://www.bahnhof.se/~misagon/>.
.\"
.TP
Jonas Minnberg (<sasq@nightmode.org>)
Jonas did intensive testing of early versions of gentoo, and eventually
persuaded me into releasing it (back around version 0.9.7 or so).
.\"
.TP
Ulf Petterson (<ulf@obsession.se>)
Ulf drew the main gentoo logo (the one shown in the About window), and also
designed the main HTML documentation's layout.
.\"
.TP
Josip Rodin (<jrodin@jagor.srce.hr>)
Maintainer of the gentoo package for Debian Linux, and also a source
of suggestions for improvements, as well as a relay for bug reports
from Debian Linux users.
.\"
.TP
Ryan Weaver (<ryanw@infohwy.com>)
Maintainer of the gentoo packages for Red Hat Linux, and probably one of
the fastest package creators out there. :)
.\"
.TP
Oliver Braun, Jim Geovedi and Pehr Johansson
Maintainers of gentoo ports to FreeBSD, OpenBSD, and NetBSD, respectively.
.PP
.\"
Thanks also to all people who have mailed me about gentoo, providing bug
reports, feature requests, and the occasional kind word. :^) It's because
of people like yourselves that we have this wonderful computer platform to
play with.
.\"
.\"
.SH "COPYRIGHT"
gentoo is released as free, open-source software, under the terms of the
GNU General Public License (GNU GPL), version 2. This license is included in the
distribution under the traditional name of
.B
COPYING,
and I suggest that you read it if you're not familiar with it. If you can't
find the file, but have Internet access, you could take a look at
.B "<http://www.gnu.org/>."
It is important to realize that the mentioned license means that there is
.B "ABSOLUTELY NO WARRANTY"
for this software.
.\"
.\"
.SH "OTHER INFO"
Some unfinished, outdated, but still pretty informative documentation is
available, in HTML format, in the
.B docs/
subdirectory in the distribution archive. If you haven't installed gentoo
from the original .tar.gz distribution archive, you might need to either
inspect the distribution you
.B "did"
use (perhaps it came as some form of "package"), or contact a system
administrator.
.PP
.\"
The GTK+ GUI toolkit that gentoo requires is available at
.B "<http://www.gtk.org/>."
gentoo uses the slightly outdated stable series, called 1.2.x. The latest
known release in that series is GTK+ 1.2.10. Because of severe performance
problems, gentoo will probably not be ported to use the current (2.0.x)
series of GTK+ any time soon.
.PP
.\"
The latest version of gentoo is always available on the official
gentoo home page, at
.B "<http://www.obsession.se/gentoo/>."
.\"
.SH "SEE ALSO"
.BR regex (7),
.BR file (1),
.BR magic (5),
.BR fstab (5),
.BR strftime (3)
.PP
.\"
Manual page section numbers in this page refer to sections on (some?)
Linux systems, your mileage will most likely vary. Try the
.BR apropos (1)
command, it might help you out.
