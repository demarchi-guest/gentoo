/*
** 1998-06-07 -	Another typical command header, this time for the CHWON native
**		command.
*/

extern gint	cmd_chown(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
