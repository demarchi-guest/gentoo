/*
** 1998-08-27 -	One just can not have too many one-line headers.
*/

extern gint	cmd_mkdir(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);

extern void	cfg_mkdir(MainInfo *min);
