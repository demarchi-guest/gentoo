/*
** 1998-08-24 -	Header for the paths config module.
*/

extern const CfgModule * cpt_describe(MainInfo *min);
extern const gchar *	 cpt_get_path(PathID id);
