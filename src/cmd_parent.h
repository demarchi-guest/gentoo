/*
** 1998-05-25 -	A very simple header file for the very simple PARENT command module.
*/

extern gint	cmd_parent(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
