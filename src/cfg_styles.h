/*
** 1998-08-14 -	Another one of those never-ending one-liner header files. The price you get
**		to pay for putting everything in a module of its own, I guess...
*/

extern const CfgModule * cst_describe(MainInfo *min);
extern StyleInfo *	 cst_get_styleinfo(void);

