/*
** 1998-09-12 -	Header file for the native MOVEAS command implementation.
*/

extern gint	cmd_moveas(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
