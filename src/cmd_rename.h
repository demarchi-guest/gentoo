/*
** 1998-05-29 -	Header for the native RENAME command. Can you say "simple"?
*/

extern gint	cmd_rename(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern void	cfg_rename(MainInfo *min);
