/*
** 1998-05-29 -	Another oneliner. This is for the CHMOD internal command.
*/

extern gint	cmd_chmod(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
