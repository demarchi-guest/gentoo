/*
** 1998-10-21 -	Header for the little quit command module.
*/

extern gint	cmd_quit(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
