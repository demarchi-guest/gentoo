/*
** 1998-09-18 -	Header file for the 'file' utility module.
*/

extern const gchar *	fle_file(MainInfo *min, const gchar *name);
