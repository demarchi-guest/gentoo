/*
** 1998-09-26 -	Header file for the output-grabbing module.
*/

extern gboolean	cgr_grab_output(MainInfo *min, const gchar *prog, GPid child, gint fd_out, gint fd_err);
