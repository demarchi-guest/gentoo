/*
** 1998-09-02 -	Header for the (pretty cool) command DirEnter.
*/

extern gint	cmd_direnter(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
