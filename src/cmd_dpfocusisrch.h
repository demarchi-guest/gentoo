/*
** 2003-11-04 -	Incremental search.
*/

extern gint	cmd_dpfocusisrch(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern void	cfg_dpfocusisrch(MainInfo *min);
