/*
** 2000-02-12 -	Header for the Join command.
*/

extern gint	cmd_join(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
