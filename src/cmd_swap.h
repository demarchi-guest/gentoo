/*
** 1998-08-08 -	Header for the SWAP module.
*/

extern gint	cmd_swap(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern gint	cmd_fromother(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern gint	cmd_toother(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
