/*
** 1998-09-12 -	Header file for the run command module.
*/

extern gint	cmd_run(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern gint	cmd_rerun(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
