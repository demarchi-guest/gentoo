/*
** Header for the menus config module.
*/

extern const CfgModule *	cmu_describe(MainInfo *min);
extern MenuInfo *		cmu_get_menuinfo(void);
