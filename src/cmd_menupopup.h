/*
** 1999-06-12 -	A little header for the menu popup command. Very early code.
*/

extern gint	cmd_menupopup(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
