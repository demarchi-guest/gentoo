/*
** 1998-09-12 -	The stream of one-line headers never does end.
*/

extern gint	cmd_getsize(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern gint	cmd_clearsize(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);

extern void	cfg_getsize(MainInfo *min);
