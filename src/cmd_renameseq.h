/*
** 2001-09-30 -	Sequential rename declaration.
*/

extern gint	cmd_renameseq(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
