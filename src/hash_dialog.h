/*
** 1999-06-19 -	New header for the rewritten hash dialog module. Now synchronous.
*/

extern const gchar *	hdl_dialog_sync_new_wait(GHashTable *hash, const gchar *title);
