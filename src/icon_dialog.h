/*
** 1999-05-24 -	Another highly specialized dialog module.
*/

extern const gchar *	idl_dialog_sync_new_wait(MainInfo *min, const gchar *path,
						const gchar *title, const gchar *current, gboolean show_progress);
