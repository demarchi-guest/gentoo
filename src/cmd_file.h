/*
** 1999-05-20 -	Header for the FileAction command.
*/

extern gint	cmd_fileaction(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
