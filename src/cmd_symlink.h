/*
** 1999-05-29 -	Symlink management command header.
*/

extern gint	cmd_symlink(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern gint	cmd_symlinkedit(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
