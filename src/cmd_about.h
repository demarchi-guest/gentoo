/*
** 1998-09-23 -	Header for the About command module. Smallish.
*/

extern gint	cmd_about(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
