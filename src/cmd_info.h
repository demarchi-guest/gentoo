/*
** 1998-12-19 -	Header for the new Info command, which might be neat.
*/

extern gint	cmd_information(MainInfo *min, DirPane *src, DirPane *dst, const CmdArg *ca);
extern void	cfg_information(MainInfo *min);
