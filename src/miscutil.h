/*
** 2004-05-01 -	Miscellanous utility functions that are needed in more than one place. A very
**		late addition, so hopefully will grow slowly.
*/

/* Return seconds from <t1> (start) to <t2> (end). */
extern gfloat	msu_diff_timeval(const GTimeVal *t1, const GTimeVal *t2);
